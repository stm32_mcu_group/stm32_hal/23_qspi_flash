/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * @file    quadspi.h
  * @brief   This file contains all the function prototypes for
  *          the quadspi.c file
  ******************************************************************************
  * @attention
  *
  * Copyright (c) 2022 STMicroelectronics.
  * All rights reserved.
  *
  * This software is licensed under terms that can be found in the LICENSE file
  * in the root directory of this software component.
  * If no LICENSE file comes with this software, it is provided AS-IS.
  *
  ******************************************************************************
  */
/* USER CODE END Header */
/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __QUADSPI_H__
#define __QUADSPI_H__

#ifdef __cplusplus
extern "C" {
#endif

/* Includes ------------------------------------------------------------------*/
#include "main.h"

/* USER CODE BEGIN Includes */

/* USER CODE END Includes */

extern QSPI_HandleTypeDef hqspi1;

/* USER CODE BEGIN Private defines */
uint8_t OWN_QSPI_Init(void);
uint8_t OWN_QSPI_EraseSector(uint32_t startAddr, uint32_t endAddr);
uint8_t OWN_QSPI_WriteMemory(uint8_t* buffer, uint32_t addr, uint32_t buff_size);
uint8_t OWN_QSPI_EnableMemoryMappedMode(void);

uint8_t OWN_QSPI_Read_ID(uint8_t* mfID, uint16_t* chipSize);
uint8_t OWN_QSPI_ReadStatus(uint8_t* statusReg);
uint8_t OWN_QSPI_Tester(void);
/* USER CODE END Private defines */

void MX_QUADSPI1_Init(void);

/* USER CODE BEGIN Prototypes */

// IS25LQ020B memory parameters

#define MEMORY_FLASH_SIZE   0x40000 // 2Mb => 256kB
#define MEMORY_BLOCK_SIZE   0x10000  // 4 sectors of 64kB
#define MEMORY_SECTOR_SIZE  0x1000   // 64 sub-sectors of 4kB
#define MEMORY_PAGE_SIZE    0x100    // 1024 pages of 256B

// IS25LQ020B commands

#define WRITE_ENABLE_CMD        0x06
#define READ_STATUS_REG_CMD     0x05
#define WRITE_STATUS_REG_CMD    0x01
#define SECTOR_ERASE_CMD        0x20 // or 0xD7
#define CHIP_ERASE_CMD          0xC7 // or 0x60
#define QUAD_IN_FAST_PROG_CMD   0x38  // or 0x32
#define QUAD_READ_IO_CMD        0xEB
#define QUAD_OUT_FAST_READ_CMD  0x6B
#define RESET_ENABLE_CMD        0x66
#define RESET_EXECUTE_CMD       0x99
#define READ_JEDEC_ID_CMD       0x9F
#define WRITE_ENABLE_CMD        0x06
#define WRITE_DISABLE_CMD       0x04

#define DUMMY_CLOCK_CYCLES_READ_QUAD 8


/* USER CODE END Prototypes */

#ifdef __cplusplus
}
#endif

#endif /* __QUADSPI_H__ */

